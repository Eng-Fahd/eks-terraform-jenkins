# This gives back object with certificate-authority among other attributes: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster#attributes-reference
data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

# This gives us object with token: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth#attributes-reference  
data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "18.0.4"

  cluster_name    = local.cluster_name
  cluster_version = var.k8s_version
  subnet_ids      = module.vpc.private_subnets

  tags = {
    Environment = "bootcamp"
    Terraform = "true"
  }

  vpc_id = module.vpc.vpc_id

  eks_managed_node_groups = {
    # blue = {}
    # green = {}
    dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types = [var.instance_type]
      labels = {
        Environment = var.env_prefix
      }
    }
  }

  fargate_profiles = {
    default = {
      name = "my-fargate-profile"
      selectors = [
        {
          namespace = "my-app"
        }
      ]
    }
  }
}