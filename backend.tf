terraform {
  backend "s3" {
    bucket = "my-bucket-exercise"
    key    = "myapp/state.tfstate"
    region  = "eu-west-3"
  }
}