resource "helm_release" "mysql" {
  name       = "my-release"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mysql"
  version    = "8.8.6" # there is a known issue for version 8.8.7+
  timeout    = "1000" # seconds

  values = [
    "${file("values.yaml")}"
  ]

}

