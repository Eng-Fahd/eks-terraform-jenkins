provider "aws" {
  # version = ">= 2.28.1"
  region  = var.region
}

provider "kubernetes" {
  # load_config_file       = "false"   ## that attribute was removed in provider release 2.0.0. It's no longer supported in newer versions.
  # use_in_cluster_config = true  ## If you're running Terraform inside the Kubernetes cluster
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

provider "helm" {
  kubernetes {
    host = data.aws_eks_cluster.cluster.endpoint
    token = data.aws_eks_cluster_auth.cluster.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  }
}